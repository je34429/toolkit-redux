import { createSlice } from '@reduxjs/toolkit';

export const counterSlice = createSlice({
    name:'counter',
    initialState:{
        counter:10
    },
    reducers:{
        increment: (state)=>{
            //permite la mutación, el toolkit luego transforma esto en un estado no mutable
            state.counter+=1
        },
        decrement: (state)=>{
            //permite la mutación, el toolkit luego transforma esto en un estado no mutable
            state.counter-=1
        },
        incrementBy: (state, action)=>{
            //permite la mutación, el toolkit luego transforma esto en un estado no mutable
            state.counter += action.payload
        },
    }
});

/*
la función createSlice retorna:
{
    actions: {increment: ƒ, decrement: ƒ, incrementByAmount: ƒ}
    caseReducers: {increment: ƒ, decrement: ƒ, incrementByAmount: ƒ}
    getInitialState: ƒ ()
    name: "counter"
    reducer: ƒ (state, action)  
}
*/
export const {increment, decrement, incrementBy} = counterSlice.actions;